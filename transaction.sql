START TRANSACTION;

INSERT INTO avis (id_avis, note, commentaire, id_film) VALUES (7, 5, 'Excellent film', 4);


UPDATE film
SET note_moyenne = (
  SELECT AVG(note)
  FROM avis
  WHERE id_film = 4
)
WHERE id_film = 4;


UPDATE film
SET coup_de_coeur = IF(note_moyenne = 5, 1, 0)
WHERE id_film = 4;


COMMIT;
ROLLBACK;
