-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 17 juil. 2024 à 19:11
-- Version du serveur : 10.4.32-MariaDB
-- Version de PHP : 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cinephoria`
--

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id_avis` int(11) NOT NULL,
  `note` int(11) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `id_film` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `avis`
--

INSERT INTO `avis` (`id_avis`, `note`, `commentaire`, `id_film`) VALUES
(5, 5, 'très bon film', 4),
(6, 4, 'bon film', 14);

--
-- Déclencheurs `avis`
--
DELIMITER $$
CREATE TRIGGER `update_film_note` AFTER INSERT ON `avis` FOR EACH ROW BEGIN
  DECLARE moyenne DECIMAL(10,2);

  -- Calculer la note moyenne pour le film
  SELECT AVG(note) INTO moyenne
  FROM avis
  WHERE id_film = NEW.id_film;

  -- Mettre à jour la note moyenne et le coup_de_coeur dans la table film
  UPDATE film
  SET note_moyenne = moyenne,
      coup_de_coeur = IF(moyenne = 5, 1, 0)
  WHERE id_film = NEW.id_film;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_film_note_on_update` AFTER UPDATE ON `avis` FOR EACH ROW BEGIN
  DECLARE moyenne DECIMAL(10,2);

  -- Calculer la note moyenne pour le film
  SELECT AVG(note) INTO moyenne
  FROM avis
  WHERE id_film = OLD.id_film;

  -- Mettre à jour la note moyenne et le coup_de_coeur dans la table film
  UPDATE film
  SET note_moyenne = moyenne,
      coup_de_coeur = IF(moyenne = 5, 1, 0)
  WHERE id_film = OLD.id_film;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `cinema`
--

CREATE TABLE `cinema` (
  `id_cinema` int(11) NOT NULL,
  `nom_cinema` varchar(45) DEFAULT NULL,
  `ville` varchar(45) NOT NULL,
  `pays` varchar(45) NOT NULL,
  `numero_telephone` int(30) DEFAULT NULL,
  `numero_rue` int(30) DEFAULT NULL,
  `rue` varchar(45) DEFAULT NULL,
  `cde_postale` int(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `cinema`
--

INSERT INTO `cinema` (`id_cinema`, `nom_cinema`, `ville`, `pays`, `numero_telephone`, `numero_rue`, `rue`, `cde_postale`) VALUES
(1, 'Cinéphonantes', 'Nantes', 'France', 123456789, 10, 'Rue de Paris', 75001),
(2, 'Cinéphodeaux', 'Bordeaux', 'France', 987654321, 20, 'Rue de Lyon', 69001),
(3, 'Cinéphoris', 'Paris', 'France', 1122334455, 30, 'Rue de Marseille', 13001),
(4, 'Cinépholouse', 'Toulouse', 'France', 2147483647, 40, 'Rue de Bordeaux', 33000),
(5, 'Cinépholille', 'Lille', 'France', 2147483647, 50, 'Rue de Toulouse', 31000),
(7, 'Cinéphoroi', 'Charleroi', 'Belgique', 23742323, 34, 'rue du cinéma', 71),
(8, 'Cinéphoège', 'Liège', 'Belgique', 23343232, 21, 'rue du cinéma', 37127);

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe` (
  `id_employe` int(11) NOT NULL,
  `nom_employe` varchar(45) DEFAULT NULL,
  `prenom_employe` varchar(45) DEFAULT NULL,
  `email_employe` varchar(45) DEFAULT NULL,
  `mdp_employe` varchar(255) DEFAULT NULL,
  `role_employe` enum('employe','administrateur') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`id_employe`, `nom_employe`, `prenom_employe`, `email_employe`, `mdp_employe`, `role_employe`) VALUES
(4, 'Moreau', 'Julien', 'julien.moreau@example.com', '$2b$10$tG8CvGVLAtoJE4B8ptGESu0n/$2y$10$UbK/Lw3t7n41gdI7gjSqo.a3k5/CrmaW9oQNoSXv129zTUG.GVRRa', 'administrateur'),
(8, 'employe', 'arthur', 'babinotarthur49@gmail.com', '$2b$10$HrAtOOFpBCyPMPw4njwc5Ocf2av6/eI7B4bLpT4g6VUz/HjNmm2VK', 'employe'),
(9, 'babinot', 'arthur', 'arthurbabinot@admin.com', '$2y$10$DTkynhmeN39IVg6c9k4sLu4GFWtJu/c1OCr8dWucMgjXV79CX4y6O', 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `film`
--

CREATE TABLE `film` (
  `id_film` int(11) NOT NULL,
  `titre` varchar(45) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `genre` varchar(100) NOT NULL,
  `age_minimum` int(10) DEFAULT NULL,
  `coup_de_coeur` tinyint(4) DEFAULT NULL,
  `note_moyenne` decimal(45,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `film`
--

INSERT INTO `film` (`id_film`, `titre`, `description`, `image`, `genre`, `age_minimum`, `coup_de_coeur`, `note_moyenne`) VALUES
(1, 'Avengers', 'Dans ce film d\'action palpitant, les plus grands super-héros de la Terre se réunissent pour former une équipe extraordinaire afin de sauver l\'humanité. Sous la direction du légendaire Iron Man, ', 'https://i.ibb.co/m0xjCZp/s-l1600.jpg', 'Science Fiction', NULL, 1, 5),
(2, 'Avatar', 'Avatar, réalisé par James Cameron, est un film de science-fiction de 2009 qui explore un monde extraterrestre luxuriant appelé Pandora, où des humains et des indigènes Na\'vi entrent en conflit', 'https://i.ibb.co/QKCW464/image.jpg', 'Science Fiction', 16, 0, 4),
(4, 'Interstellar', 'dirigé par Christopher Nolan en 2014, est une épopée de science-fiction qui suit un groupe d\'explorateurs spatiaux à la recherche d\'une nouvelle planète habitable pour l\'humanité, ', 'https://i.ibb.co/vJS99KF/INTERSTELLAR.jpg', 'Science Fiction', NULL, 1, 5),
(5, 'Joker', 'Joker, réalisé par Todd Phillips en 2019, est un thriller psychologique qui retrace la transformation d\'Arthur Fleck, un comédien raté et marginalisé, en l\'iconique criminel du même nom dans Gotham', 'https://i.ibb.co/TMnbBZs/71-XWri-UUKIL-AC-UF1000-1000-QL80.jpg', 'Drame', 18, 0, 1),
(8, 'Insidious', 'Insidious\" est un film d\'horreur captivant qui plonge les spectateurs dans un monde surnaturel terrifiant, explorant les ténèbres de l\'esprit humain avec des rebondissements inattendu', 'https://i.ibb.co/QpmxxHJ/712-7pkiqw-L-AC-UF1000-1000-QL80.jpg', 'Horreur', 18, 0, 2),
(11, 'Annabelle', 'mettant en scène une poupée possédée par un esprit maléfique. Il suit un couple confronté à des événements terrifiants après avoir accueilli la poupée dans leur foyer', 'https://i.ibb.co/6PKqzy3/1306692953.jpg', 'Horreur', 18, 0, 5),
(13, 'Titanic', ' Le film suit les destins croisés de Jack et Rose, deux passagers de classes sociales différentes, qui tombent amoureux à bord du navire lors de son voyage', 'https://i.ibb.co/0DCMZ1P/Titanic.jpg', 'Romantique', 18, 1, 5),
(14, 'Intouchables', 'l\'histoire émouvante et drôle de l\'amitié entre un riche tétraplégique et son auxiliaire de vie, issu de la banlieue.', 'https://i.ibb.co/GTQCHf7/5a7350bec667d687a8e2e8c8509a27ba.jpg', 'Drame', NULL, 0, 4);

-- --------------------------------------------------------

--
-- Structure de la table `incident`
--

CREATE TABLE `incident` (
  `id_incident` int(11) NOT NULL,
  `id_salle` int(11) NOT NULL,
  `commentaire` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `incident`
--

INSERT INTO `incident` (`id_incident`, `id_salle`, `commentaire`) VALUES
(1, 2, 'Siège numéro 68, déchirure sur le coté droit'),
(3, 6, 'Siège numéro 12, Grosse tâche de soda sur l\'assise '),
(19, 5, 'Marche déféctueuse, dangeureuse '),
(20, 3, 'Siège numéro 13, très sal');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id_reservation` int(11) NOT NULL,
  `date_reservation` datetime DEFAULT NULL,
  `nombre_personnes` int(32) DEFAULT NULL,
  `prix_total` decimal(10,0) DEFAULT NULL,
  `statut` enum('En attente','Confirmée','Annulée') DEFAULT NULL,
  `id_seance` int(11) DEFAULT NULL,
  `id_utilisateur_res` int(100) NOT NULL,
  `id_film` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`id_reservation`, `date_reservation`, `nombre_personnes`, `prix_total`, `statut`, `id_seance`, `id_utilisateur_res`, `id_film`) VALUES
(52, '2024-07-31 15:45:00', 14, 98, 'Confirmée', 22, 19, 5),
(53, '2024-08-14 20:50:00', 3, 30, 'Confirmée', 39, 19, 8);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `reservation_mobile`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `reservation_mobile` (
`id_reservation` int(11)
,`nombre_personnes` int(32)
,`id_film` int(11)
,`nom_film` varchar(45)
,`affiche_film` varchar(255)
,`date` datetime
,`heure_debut` varchar(10)
,`heure_fin` varchar(10)
,`numero_salle` int(32)
,`email_utilisateur` varchar(45)
);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `id_salle` int(11) NOT NULL,
  `num_salle` int(32) DEFAULT NULL,
  `capacite` int(32) DEFAULT NULL,
  `type_projection` varchar(45) DEFAULT NULL,
  `id_cinema` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id_salle`, `num_salle`, `capacite`, `type_projection`, `id_cinema`) VALUES
(2, 2, 150, '3D', 2),
(3, 3, 120, 'IMAX', 3),
(5, 5, 80, 'Dolby', 4),
(6, 9, 120, '2D', 2),
(8, 1, 120, '3D', 5),
(9, 10, 120, '2D', 5),
(10, 11, 150, '4Dx', 5),
(11, 12, 150, '4Dx', 2),
(12, 13, 150, '3D', 2),
(13, 14, 150, '2D', 2),
(14, 15, 120, '3D', 7),
(15, 16, 120, '4Dx', 7);

-- --------------------------------------------------------

--
-- Structure de la table `seance`
--

CREATE TABLE `seance` (
  `id_seance` int(11) NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `id_film_seance` int(11) DEFAULT NULL,
  `id_salle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `seance`
--

INSERT INTO `seance` (`id_seance`, `date_debut`, `date_fin`, `id_film_seance`, `id_salle`) VALUES
(22, '2024-07-31 17:45:00', '2024-07-31 19:45:00', 5, 3),
(23, '2024-08-17 20:50:00', '2024-07-17 22:50:00', 2, 6),
(24, '2024-07-22 22:50:00', '2024-07-17 00:50:00', 4, 6),
(25, '2024-08-17 10:50:00', '2024-07-17 12:50:00', 8, 3),
(26, '2024-08-17 10:50:00', '2024-07-17 12:50:00', 4, 5),
(27, '2024-08-17 10:50:00', '2024-07-17 12:50:00', 13, 3),
(28, '2024-08-20 14:50:00', '2024-07-17 16:50:00', 11, 5),
(29, '2024-08-20 16:50:00', '2024-07-17 18:50:00', 11, 5),
(30, '2024-08-20 18:50:00', '2024-07-17 20:50:00', 14, 3),
(31, '2024-08-20 18:50:00', '2024-07-17 20:50:00', 14, 15),
(32, '2024-08-20 18:50:00', '2024-07-17 20:50:00', 14, 14),
(33, '2024-08-20 18:50:00', '2024-07-17 20:50:00', 14, 13),
(34, '2024-08-20 18:50:00', '2024-07-17 20:50:00', 14, 12),
(35, '2024-08-14 18:50:00', '2024-07-14 20:50:00', 1, 12),
(36, '2024-08-14 18:50:00', '2024-07-14 20:50:00', 1, 9),
(37, '2024-08-14 18:50:00', '2024-07-14 20:50:00', 1, 2),
(38, '2024-08-14 18:50:00', '2024-07-14 20:50:00', 1, 8),
(39, '2024-08-14 22:50:00', '2024-07-14 00:50:00', 8, 11),
(40, '2024-08-14 22:50:00', '2024-07-14 00:50:00', 8, 12),
(41, '2024-08-14 22:50:00', '2024-07-14 00:50:00', 8, 13),
(42, '2024-08-14 22:50:00', '2024-07-14 00:50:00', 8, 15);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `nom_utilisateur` varchar(45) DEFAULT NULL,
  `prenom_utilisateur` varchar(45) DEFAULT NULL,
  `email_utilisateur` varchar(45) DEFAULT NULL,
  `mdp_utilisateur` varchar(60) DEFAULT NULL,
  `role_utilisateur` enum('Client') DEFAULT NULL,
  `compte_active` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `nom_utilisateur`, `prenom_utilisateur`, `email_utilisateur`, `mdp_utilisateur`, `role_utilisateur`, `compte_active`) VALUES
(19, 'babinot', 'arthur', 'babinotarthur72@gmail.com', '$2b$10$3cWY.ll0.ZwEi.M7TA48tOoFoQL8fYqGfCCI/FmTFl9TeL5zwbnPm', 'Client', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_has_film`
--

CREATE TABLE `utilisateur_has_film` (
  `UTILISATEUR_id_utilisateur` int(11) NOT NULL,
  `UTILISATEUR_RESERVATION_id_reservation` int(11) NOT NULL,
  `UTILISATEUR_AVIS_id_avis` int(11) NOT NULL,
  `FILM_id_film` int(11) NOT NULL,
  `FILM_SEANCE_id_seance` int(11) NOT NULL,
  `FILM_AVIS_id_avis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `utilisateur_has_film`
--

INSERT INTO `utilisateur_has_film` (`UTILISATEUR_id_utilisateur`, `UTILISATEUR_RESERVATION_id_reservation`, `UTILISATEUR_AVIS_id_avis`, `FILM_id_film`, `FILM_SEANCE_id_seance`, `FILM_AVIS_id_avis`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 2, 2, 2, 2, 2),
(3, 3, 3, 3, 3, 3),
(4, 4, 4, 4, 4, 4),
(5, 5, 5, 5, 5, 5);



--
-- Structure de la vue `reservation_mobile`
--
DROP TABLE IF EXISTS `reservation_mobile`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reservation_mobile`  AS SELECT `r`.`id_reservation` AS `id_reservation`, `r`.`nombre_personnes` AS `nombre_personnes`, `f`.`id_film` AS `id_film`, `f`.`titre` AS `nom_film`, `f`.`image` AS `affiche_film`, `s`.`date_debut` AS `date`, date_format(`s`.`date_debut`,'%H:%i') AS `heure_debut`, date_format(`s`.`date_fin`,'%H:%i') AS `heure_fin`, `sa`.`num_salle` AS `numero_salle`, `u`.`email_utilisateur` AS `email_utilisateur` FROM ((((`reservation` `r` join `seance` `s` on(`r`.`id_seance` = `s`.`id_seance`)) join `film` `f` on(`s`.`id_film_seance` = `f`.`id_film`)) join `salle` `sa` on(`s`.`id_salle` = `sa`.`id_salle`)) join `utilisateur` `u` on(`r`.`id_utilisateur_res` = `u`.`id_utilisateur`)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_avis_intranet`
--
DROP TABLE IF EXISTS `vue_avis_intranet`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_avis_intranet`  AS SELECT `avis`.`id_avis` AS `id_avis`, `avis`.`note` AS `note`, `avis`.`commentaire` AS `commentaire`, `film`.`id_film` AS `id_film`, `film`.`titre` AS `titre` FROM (`avis` join `film` on(`avis`.`id_film` = `film`.`id_film`)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_films_intranet`
--
DROP TABLE IF EXISTS `vue_films_intranet`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_films_intranet`  AS SELECT `film`.`id_film` AS `id_film`, `film`.`titre` AS `titre`, `film`.`description` AS `description`, `film`.`genre` AS `genre`, `film`.`age_minimum` AS `age_minimum` FROM `film` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_incidents`
--
DROP TABLE IF EXISTS `vue_incidents`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_incidents`  AS SELECT `i`.`id_incident` AS `id_incident`, `i`.`id_salle` AS `id_salle`, `i`.`commentaire` AS `commentaire`, `s`.`num_salle` AS `num_salle` FROM (`incident` `i` join `salle` `s` on(`i`.`id_salle` = `s`.`id_salle`)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_reservation`
--
DROP TABLE IF EXISTS `vue_reservation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_reservation`  AS SELECT `seance`.`id_seance` AS `id_seance`, `seance`.`date_debut` AS `date`, date_format(`seance`.`date_debut`,'%H:%i') AS `heure_debut`, date_format(`seance`.`date_fin`,'%H:%i') AS `heure_fin`, `film`.`id_film` AS `id_film`, `film`.`titre` AS `nom_film`, `film`.`genre` AS `genre`, `salle`.`type_projection` AS `type_projection`, `salle`.`capacite` AS `capacite`, `cinema`.`nom_cinema` AS `nom_cinema`, `salle`.`num_salle` AS `num_salle` FROM (((`seance` join `film` on(`seance`.`id_film_seance` = `film`.`id_film`)) join `salle` on(`seance`.`id_salle` = `salle`.`id_salle`)) join `cinema` on(`salle`.`id_cinema` = `cinema`.`id_cinema`)) WHERE `seance`.`date_debut` > current_timestamp() ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_reservations_par_film`
--
DROP TABLE IF EXISTS `vue_reservations_par_film`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_reservations_par_film`  AS SELECT `f`.`id_film` AS `id_film`, `f`.`titre` AS `nom_film`, count(`r`.`id_reservation`) AS `nombre_reservations` FROM ((`film` `f` left join `seance` `s` on(`f`.`id_film` = `s`.`id_film_seance`)) left join `reservation` `r` on(`s`.`id_seance` = `r`.`id_seance`)) GROUP BY `f`.`id_film`, `f`.`titre` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_reservations_utilisateurs`
--
DROP TABLE IF EXISTS `vue_reservations_utilisateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_reservations_utilisateurs`  AS SELECT `r`.`id_reservation` AS `id_reservation`, `r`.`date_reservation` AS `date_reservation`, `r`.`nombre_personnes` AS `nombre_personnes`, `r`.`prix_total` AS `prix_total`, `r`.`statut` AS `statut`, `u`.`id_utilisateur` AS `id_utilisateur`, `u`.`nom_utilisateur` AS `nom_utilisateur`, `u`.`prenom_utilisateur` AS `prenom_utilisateur`, `u`.`email_utilisateur` AS `email_utilisateur`, `f`.`id_film` AS `id_film` FROM ((`reservation` `r` join `utilisateur` `u` on(`r`.`id_utilisateur_res` = `u`.`id_utilisateur`)) join `film` `f` on(`r`.`id_film` = `f`.`id_film`)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_salles_intranet`
--
DROP TABLE IF EXISTS `vue_salles_intranet`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_salles_intranet`  AS SELECT `salle`.`id_salle` AS `id_salle`, `salle`.`num_salle` AS `num_salle`, `salle`.`capacite` AS `capacite`, `salle`.`type_projection` AS `type_projection` FROM `salle` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_seances`
--
DROP TABLE IF EXISTS `vue_seances`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_seances`  AS SELECT `seance`.`id_seance` AS `id_seance`, cast(`seance`.`date_debut` as date) AS `date_seance`, cast(`seance`.`date_debut` as time) AS `heure_debut`, cast(`seance`.`date_fin` as time) AS `heure_fin`, `salle`.`type_projection` AS `type_projection`, `film`.`titre` AS `nom_film` FROM ((`seance` join `salle` on(`seance`.`id_salle` = `salle`.`id_salle`)) join `film` on(`seance`.`id_film_seance` = `film`.`id_film`)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_seances_detail`
--
DROP TABLE IF EXISTS `vue_seances_detail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_seances_detail`  AS SELECT `seance`.`id_seance` AS `id_seance`, `seance`.`date_debut` AS `date`, date_format(`seance`.`date_debut`,'%H:%i') AS `heure_debut`, date_format(`seance`.`date_fin`,'%H:%i') AS `heure_fin`, `film`.`titre` AS `nom_film`, `film`.`genre` AS `genre`, `salle`.`type_projection` AS `type_projection`, `cinema`.`nom_cinema` AS `nom_cinema` FROM (((`seance` join `film` on(`seance`.`id_film_seance` = `film`.`id_film`)) join `salle` on(`seance`.`id_salle` = `salle`.`id_salle`)) join `cinema` on(`salle`.`id_cinema` = `cinema`.`id_cinema`)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_utilisateurs_employes`
--
DROP TABLE IF EXISTS `vue_utilisateurs_employes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_utilisateurs_employes`  AS SELECT concat('U',`utilisateur`.`id_utilisateur`) AS `id`, `utilisateur`.`nom_utilisateur` AS `nom`, `utilisateur`.`prenom_utilisateur` AS `prenom`, `utilisateur`.`email_utilisateur` AS `email`, `utilisateur`.`role_utilisateur` AS `role`, `utilisateur`.`mdp_utilisateur` AS `mot_de_passe` FROM `utilisateur`union select concat('E',`employe`.`id_employe`) AS `id`,`employe`.`nom_employe` AS `nom`,`employe`.`prenom_employe` AS `prenom`,`employe`.`email_employe` AS `email`,`employe`.`role_employe` AS `role`,`employe`.`mdp_employe` AS `mot_de_passe` from `employe`  ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`id_avis`),
  ADD KEY `fk_id_film` (`id_film`);

--
-- Index pour la table `cinema`
--
ALTER TABLE `cinema`
  ADD PRIMARY KEY (`id_cinema`) USING BTREE;

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id_employe`) USING BTREE;

--
-- Index pour la table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id_film`) USING BTREE;

--
-- Index pour la table `incident`
--
ALTER TABLE `incident`
  ADD PRIMARY KEY (`id_incident`),
  ADD KEY `fk_id_salle` (`id_salle`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id_reservation`),
  ADD KEY `fk_reservation_seance` (`id_seance`),
  ADD KEY `fk_reservation_utilisateur` (`id_utilisateur_res`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id_salle`) USING BTREE,
  ADD KEY `fk_salle_cinema` (`id_cinema`);

--
-- Index pour la table `seance`
--
ALTER TABLE `seance`
  ADD PRIMARY KEY (`id_seance`) USING BTREE,
  ADD KEY `fk_seance_film` (`id_film_seance`),
  ADD KEY `fk_SEANCE_SALLE` (`id_salle`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id_utilisateur`) USING BTREE;

--
-- Index pour la table `utilisateur_has_film`
--
ALTER TABLE `utilisateur_has_film`
  ADD PRIMARY KEY (`UTILISATEUR_id_utilisateur`,`UTILISATEUR_RESERVATION_id_reservation`,`UTILISATEUR_AVIS_id_avis`,`FILM_id_film`,`FILM_SEANCE_id_seance`,`FILM_AVIS_id_avis`),
  ADD KEY `fk_UTILISATEUR_has_FILM_FILM1_idx` (`FILM_id_film`,`FILM_SEANCE_id_seance`,`FILM_AVIS_id_avis`),
  ADD KEY `fk_UTILISATEUR_has_FILM_UTILISATEUR1_idx` (`UTILISATEUR_id_utilisateur`,`UTILISATEUR_RESERVATION_id_reservation`,`UTILISATEUR_AVIS_id_avis`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `avis`
--
ALTER TABLE `avis`
  MODIFY `id_avis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `cinema`
--
ALTER TABLE `cinema`
  MODIFY `id_cinema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
  MODIFY `id_employe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `film`
--
ALTER TABLE `film`
  MODIFY `id_film` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `incident`
--
ALTER TABLE `incident`
  MODIFY `id_incident` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id_reservation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `id_salle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `seance`
--
ALTER TABLE `seance`
  MODIFY `id_seance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `avis`
--
ALTER TABLE `avis`
  ADD CONSTRAINT `fk_id_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id_film`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `incident`
--
ALTER TABLE `incident`
  ADD CONSTRAINT `fk_id_salle` FOREIGN KEY (`id_salle`) REFERENCES `salle` (`id_salle`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_reservation_seance` FOREIGN KEY (`id_seance`) REFERENCES `seance` (`id_seance`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reservation_utilisateur` FOREIGN KEY (`id_utilisateur_res`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `salle`
--
ALTER TABLE `salle`
  ADD CONSTRAINT `fk_salle_cinema` FOREIGN KEY (`id_cinema`) REFERENCES `cinema` (`id_cinema`);

--
-- Contraintes pour la table `seance`
--
ALTER TABLE `seance`
  ADD CONSTRAINT `fk_SEANCE_SALLE` FOREIGN KEY (`id_salle`) REFERENCES `salle` (`id_salle`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_seance_film` FOREIGN KEY (`id_film_seance`) REFERENCES `film` (`id_film`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
